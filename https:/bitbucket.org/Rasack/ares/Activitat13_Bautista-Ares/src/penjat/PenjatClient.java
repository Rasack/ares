package penjat;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class PenjatClient {
	public static void main(String[] args) throws UnknownHostException, IOException, ClassNotFoundException {
		Socket client = new Socket("localhost", 6464);
		ObjectInputStream inObjecte = new ObjectInputStream(client.getInputStream());
		try {
			EstatPartida partida = (EstatPartida) inObjecte.readObject();
			partida.mostrarPartida();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		client.close();
	}
}
