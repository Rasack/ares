package penjat;

import java.awt.List;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class PenjatServer {
	public static void main(String[] args) throws IOException {
		int numeroPort = 6464;
		ServerSocket server = new ServerSocket(numeroPort);
		Socket client = server.accept();
		ObjectOutputStream outObjecte = new ObjectOutputStream(client.getOutputStream());

		InputStream entrada = null;
		entrada = client.getInputStream();
		DataInputStream Entrada = new DataInputStream(entrada);
		
		String[] paraules={"dishonored","mass effect","shadow of mordor","world of warcraft"};
		int numeroParaula = (int) (Math.random() * paraules.length);
		char[] paraula = new char[paraules[numeroParaula].length()];
		
		paraula = paraules[numeroParaula].toCharArray();

		for (int i = 0; i < paraula.length; i++) {
			if(paraula[i]!=' ')
				paraula[i] = '*';
		}
		
		EstatPartida partida = new EstatPartida();
		partida.setEstatPartida("En curs");
		partida.setParaula(paraula);
		partida.setVidaRestant(6);
		
		outObjecte.writeObject(partida);
		String lletra = Entrada.readUTF();
    	System.out.println("Recibo: "+lletra);
		
		outObjecte.close();
		server.close();
		
		
	}
}
