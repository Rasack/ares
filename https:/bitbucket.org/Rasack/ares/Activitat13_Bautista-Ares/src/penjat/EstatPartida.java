package penjat;

import java.io.Serializable;

public class EstatPartida implements Serializable{
	private static final long serialVersionUID = 1L;
	private char ultimaResposta;
	private boolean resultatUltimaResposta;
	private char[] lletresDites;
	private char[] paraula;
	private int vidaRestant;
	private String estatPartida;
	
	public EstatPartida() {	}

	public char getUltimaResposta() {
		return ultimaResposta;
	}

	public void setUltimaResposta(char ultimaResposta) {
		this.ultimaResposta = ultimaResposta;
	}

	public boolean isResultatUltimaResposta() {
		return resultatUltimaResposta;
	}

	public void setResultatUltimaResposta(boolean resultatUltimaResposta) {
		this.resultatUltimaResposta = resultatUltimaResposta;
	}

	public char[] getLletresDites() {
		return lletresDites;
	}

	public void setLletresDites(char[] lletresDites) {
		this.lletresDites = lletresDites;
	}

	public char[] getParaula() {
		return paraula;
	}

	public void setParaula(char[] paraula) {
		this.paraula = paraula;
	}

	public int getVidaRestant() {
		return vidaRestant;
	}

	public void setVidaRestant(int vidaRestant) {
		this.vidaRestant = vidaRestant;
	}

	public String getEstatPartida() {
		return estatPartida;
	}

	public void setEstatPartida(String estatPartida) {
		this.estatPartida = estatPartida;
	}
	
	public void mostrarPartida(){
		if(ultimaResposta == ' '){
			System.out.println("La teva ultima paraula va ser: "+ultimaResposta);
			if(resultatUltimaResposta){
				System.out.println("Aquesta paraula era correcte.");
			}else{
				System.out.println("Aquesta paraula era incorrecte.");
			}
			System.out.print("Aquestes son les lletres dites: ");
			System.out.println(lletresDites);
		}
		System.out.print("Estat de la paraula: ");
		System.out.println(paraula);
		System.out.println("Vides restants: " + vidaRestant);
		System.out.println("Estat de la partida: " + estatPartida);
	}
	 
}
